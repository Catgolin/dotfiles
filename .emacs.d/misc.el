;; Loading evil-mode
(require 'evil)
(evil-mode 1)
;; Allows to fold blocks of codes using zc
(add-hook 'prog-mode-hook 'origami-mode)

;; Remove bell sounds
(setq visible-bell t)
(setq ring-bell-function 'ignore)

;; Add line numbers
(global-linum-mode t)
(add-hook 'org-agenda-mode-hook 'linum-mode)

;; Encoding
(prefer-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8-auto-unix)

;; A sentence doesn't end with 2 spaces
(setq sentence-end-double-space nil)

;; PDF view
(setq doc-view-continuous t)

;; RSS
(setq elfeed-db-directory (expand-file-name "rss" user-emacs-directory)
  elfeed-show-entry-switch 'display-buffer)

;; Spelling
(add-to-list 'ispell-skip-region-alist '("#\\+" . ":"))
(add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
(add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_SRC" . "#\\+END_SRC"))
(setq ispell-silently-savep t)

;; Projectile
(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;; Bibtex from DOI
(defun my/get-bibtex-from-doi (doi)
  "Get a BibTeX entry from the DOI"
  (interactive "MDOI: ")
  (let ((url-mime-accept-string "text/bibliography;style=bibtex"))
	(with-current-buffer
		(url-retrieve-synchronously
		 (format "http://dx.doi.org/%s"
				 (replace-regexp-in-string "http://dx.doi.org/" "" doi)))
	  (switch-to-buffer (current-buffer))
	  (goto-char (point-max))
	  (setq bibtex-entry
			(buffer-substring
			 (string-match "@" (buffer-string))
			 (point)))
	  (kill-buffer (current-buffer))))
  (insert (decode-coding-string bibtex-entry 'utf-8))
  (bibtex-fill-entry))

(defun my/get-bibtex-from-isbn (isbn)
  "Get a BibTeX entry from Sudoc by the ISBN"
  (interactive "MISBN: ")
  (let ((url-mime-accept-string "text/json"))
	(with-current-buffer
		(url-retrieve-synchronously
		 (format "https://www.sudoc.fr/services/isbn2ppn/%s" isbn))
	  (switch-to-buffer (current-buffer))
	  (goto-char (point-max))
	  (setq json-text (string-match "{*}" (buffer-string)))
	  (message "%" json-text)
	  (json-parse (json-text))
	  (message "Test 1")
	  (message (string-match "\"ppn\":\"[^}]+\"" (buffer-string)))
	  (message "Test 2")
	  (setq ppn (string-match "\"ppn\":\"[[:alnum:]]+\"" (buffer-string)))
	  (message "ppn: %s" ppn)
	  (insert (decode-coding-string ppn 'utf-8))))
  (bibtex-fill-entry))
