(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-use-fuzzy t)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-enabled-themes '(manoj-dark))
 '(elfeed-feeds '("http://arxiv.org/rss/cs.CY"))
 '(global-auto-complete-mode t)
 '(indent-tabs-mode nil)
 '(line-number-mode t)
 '(line-spacing 7)
 '(mocha-command "/usr/bin/mocha")
 '(mweb-tags '(js-mode "<script[^>]*>" "</script>"))
 '(org-agenda-current-time-string "(now)")
 '(org-agenda-custom-commands
   '(("n" "Agenda and all TODOs"
      ((agenda "" nil)
       (alltodo ""
                ((todo "DONE|TODO|NEXT"))))
      nil)
     ("R" "Weekly Review"
      ((agenda ""
               ((org-agenda-skip-scheduled-if-done nil)
                (org-agenda-skip-timestamp-if-done nil)
                (org-agenda-span 7)))
       (alltodo ""
                ((todo "DONE|TODO|NEXT"))))
      nil)))
 '(org-agenda-diary-file "~/org/notes.org")
 '(org-agenda-files
   '("~/Documents/PERSONNEL/ORG-inbox.org" "/home/catgolin/Documents/PROFESSIONNEL/SPHERE/PhD/PhD-CARTIER.org" "/home/catgolin/Documents/PROFESSIONNEL/SPHERE/SPHERE-Taches.org"))
 '(org-agenda-include-diary t)
 '(org-agenda-skip-deadline-if-done t)
 '(org-agenda-skip-scheduled-if-done t)
 '(org-agenda-sorting-strategy
   '((agenda habit-down time-up priority-down category-keep)
     (todo timestamp-up deadline-up scheduled-up priority-down todo-state-up)
     (tags priority-down category-keep)
     (search category-keep)))
 '(org-agenda-time-grid '((daily weekly require-timed remove-match) nil "-----" ""))
 '(org-agenda-todo-ignore-deadlines t)
 '(org-agenda-todo-ignore-scheduled 'future)
 '(org-archive-location "~/org/archive.org::/datetree* From %s")
 '(org-babel-python-command "python3")
 '(org-beamer-environments-extra '(("onlyenv" "O" "\\begin{onlyenv}%a" "\\end{onlyenv}")))
 '(org-beamer-frame-level 2)
 '(org-beamer-theme "default")
 '(org-caldav-calendar-id "personal")
 '(org-caldav-delete-calendar-entries 'always)
 '(org-capture-templates
   '(("d" "Distractions" entry
      (file+headline "~/Documents/PERSONNEL/ORG-inbox.org" "Distractions")
      "* %?
%T")
     ("r" "Rendez-vous" entry
      (file+headline "~/Documents/PERSONNEL/ORG-inbox.org" "Rendez-vous")
      "* %?
%T")))
 '(org-cite-csl-locales-dir "~/org/csl/locales")
 '(org-cite-csl-styles-dir "~/org/")
 '(org-cite-export-processors '((t csl "~/org/citation-style.csl" nil)))
 '(org-cite-global-bibliography '("~/org/bibliography.bib"))
 '(org-columns-default-format-for-agenda "%25ITEM %TODO %3PRIORITY %ALLTAGS %DEADLINE %SCHEDULED")
 '(org-directory "~/Documents/PERSONNEL/Organisation/Agenda")
 '(org-emphasis-alist
   '(("*" bold)
     ("/" italic)
     ("_" underline)
     ("=" org-verbatim verbatim)
     ("~" org-code verbatim)
     ("+"
      (:strike-through t))))
 '(org-entities-user '(("venus" "\\venus" nil "&A50;" "" "♀" "♀")))
 '(org-export-default-language "fr")
 '(org-export-exclude-tags '("noexport" "ARCHIVE"))
 '(org-export-headline-levels 4)
 '(org-export-with-author t)
 '(org-export-with-creator nil)
 '(org-export-with-email t)
 '(org-export-with-priority nil)
 '(org-export-with-smart-quotes t)
 '(org-export-with-tags nil)
 '(org-export-with-todo-keywords nil)
 '(org-file-apps
   '((auto-mode . emacs)
     (directory . emacs)
     ("\\.mm\\'" . default)
     ("\\.x?html?\\'" . default)
     ("\\.pdf\\'" . "evince %s")))
 '(org-fold-show-context-detail
   '((agenda . local)
     (bookmark-jump . lineage)
     (isearch . lineage)
     (default . ancestors)
     (tags-tree . ancestors-full)))
 '(org-hide-leading-stars t)
 '(org-html-indent t)
 '(org-html-tag-class-prefix "_")
 '(org-html-with-latex 'mathjax)
 '(org-icalendar-alarm-time 2)
 '(org-icalendar-combined-agenda-file "/tmp/org-caldav-RdgsgW")
 '(org-icalendar-exclude-tags '("ARCHIVE"))
 '(org-icalendar-use-deadline '(todo-due))
 '(org-icalendar-use-scheduled '(event-if-not-todo event-if-todo event-if-todo-not-done))
 '(org-image-actual-width t)
 '(org-koma-letter-class-option-file "")
 '(org-koma-letter-url "https://catgolin.eu")
 '(org-koma-letter-use-email t)
 '(org-koma-letter-use-foldmarks nil)
 '(org-koma-letter-use-from-logo t)
 '(org-koma-letter-use-phone nil)
 '(org-koma-letter-use-place nil)
 '(org-koma-letter-use-url t)
 '(org-latex-classes
   '(("lettre" "\\documentclass[firstfoot=false, enlargefirstpage]{scrlttr2}
\\usepackage[french]{babel}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]")
     ("beamer" "\\documentclass[12pt]{beamer}"
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\begin{frame}[fragile]\\frametitle{%s}" "\\end{frame}" "\\begin{frame}[fragile]\\frametitle{%s}" "\\end{frame}"))
     ("lettre" "\\documentclass[firstfoot=false, enlargefirstpage]{scrlttr2}
\\usepackage[french]{babel}
\\usepacakage{setspace}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]")
     ("lettre" "\\documentclass[firstfoot=false, enlargefirstpage]{scrlttr2}
\\usepackage{setspace}")
     ("default-koma-letter" "\\documentclass[11pt]{scrlttr2}
\\usepackage{setspace}")
     ("article" "\\documentclass[12pt]{article}
\\usepackage{setspace}
\\onehalfspace"
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
      ("\\paragraph{%s}" . "\\paragraph*{%s}")
      ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
     ("report-nopart" "\\documentclass[12pt]{report}
\\usepackage{setspace}
\\onehalfspace"
      ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
      ("\\paragraph{%s}" . "\\paragraph*{%s}")
      ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
     ("report" "\\documentclass[12pt]{report}
\\usepackage{setspace}
\\onehalfspace"
      ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
     ("book" "\\documentclass[12pt, oneside]{book}
\\usepackage{setspace}
\\onehalfspace"
      ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
     ("sphere-document" "\\documentclass[12pt]{sphere-document}
\\usepackage{setspace}
\\onehalfspace"
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
      ("\\paragraph{%s}" . "\\paragraph*{%s}")
      ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))
 '(org-latex-default-packages-alist
   '(("LAE, LGR, T2A, T1" "fontenc" t
      ("pdflatex"))
     ("" "graphicx" t nil)
     ("" "grffile" t nil)
     ("" "longtable" nil nil)
     ("" "amsmath" t nil)
     ("" "textcomp" t nil)
     ("" "amssymb" t nil)
     ("AUTO" "babel" t nil)
     ("" "imakeidx" nil nil)))
 '(org-latex-packages-alist
   '(("" "textalpha" t)
     ("" "arabtex" t)
     ("" "utf8" nil)
     ("newfloat" "minted" nil)
     ("hidelinks" "hyperref" nil)
     ("" "wasysym" nil)
     ("" "setspace" nil)
     ("" "ulem" nil)))
 '(org-latex-pdf-process
   '("mv %f %o/%b.skt; skt %o/%b.skt %f; %latex -interaction nonstopmode -shell-escape -output-directory %o %f" "%latex -interaction nonstopmode -shell-escape -output-directory %o %f" "%latex -interaction nonstopmode -shell-escape -output-directory %o %f"))
 '(org-latex-src-block-backend 'minted)
 '(org-latex-text-markup-alist
   '((bold . "\\textbf{%s}")
     (code . protectedtexttt)
     (italic . "\\textit{%s}")
     (strike-through . "\\sout{%s}")
     (underline . "\\uline{%s}")
     (verbatim . protectedtexttt)))
 '(org-latex-to-mathml-convert-command "latexmlmath \"%i\" --presentationmathml=%o")
 '(org-latex-toc-command "\\tableofcontents
\\newpage
")
 '(org-log-into-drawer t)
 '(org-log-repeat 'note)
 '(org-modules
   '(ol-bbdb ol-bibtex ol-docview ol-doi ol-eww ol-gnus org-habit ol-info ol-irc ol-mhe ol-rmail ol-w3m))
 '(org-odd-levels-only t)
 '(org-pomodoro-length 25)
 '(org-pomodoro-long-break-frequency 3)
 '(org-pomodoro-long-break-length 30)
 '(org-pomodoro-manual-break t)
 '(org-pomodoro-short-break-length 5)
 '(org-publish-project-alist
   '(("Mémoire" :base-directory "~/Documents/Memoire/Chapitres" :publishing-directory "~/Documents/Memoire/out" :publishing-function org-latex-publish-to-latex :body-only t :makeindex t)
     ("Test" :base-directory "~/Documents/Nouveau/TESTS/TEST-org-publishing/root" :publishing-directory "~/Documents/Nouveau/TESTS/TEST-org-publishing/public" :publishing-function org-html-publish-to-html :recursive t)))
 '(org-refile-allow-creating-parent-nodes 'confirm)
 '(org-refile-targets '((org-agenda-files :tag . "refile_dest")))
 '(org-safe-remote-resources
   '("\\`https://manuscrits\\.catgolin\\.eu\\(?:/\\|\\'\\)" "\\`https://eida\\.obspm\\.fr\\(?:/\\|\\'\\)" "\\`https://eida\\.obspm\\.fr/iiif/2/wit332_img476_0001\\.jpg/2031,863,372,420/96,/0/default\\.jpg\\'" "\\`https://ia802200\\.us\\.archive\\.org\\(?:/\\|\\'\\)" "\\`https://content\\.staatsbibliothek-berlin\\.de\\(?:/\\|\\'\\)" "\\`https://www\\.ancientegyptfoundation\\.org/images/otto_neugebauer\\.jpg\\'" "\\`https://digi\\.vatlib\\.it\\(?:/\\|\\'\\)" "\\`https://gallica\\.bnf\\.fr\\(?:/\\|\\'\\)" "\\`https://www\\.e-rara\\.ch\\(?:/\\|\\'\\)" "\\`https://lib\\.is\\(?:/\\|\\'\\)" "\\`https://sammlungen\\.ub\\.uni-frankfurt\\.de\\(?:/\\|\\'\\)" "\\`https://api\\.digitale-sammlungen\\.de\\(?:/\\|\\'\\)" "\\`https://upload\\.wikimedia\\.org\\(?:/\\|\\'\\)" "\\`https://commons\\.wikimedia\\.org\\(?:/\\|\\'\\)"))
 '(org-startup-folded 'content)
 '(org-startup-shrink-all-tables t)
 '(package-selected-packages
   '(pylint memoize jdee multi-web-mode web-mode org-super-agenda guess-language color-theme zotxt jedi auto-complete projectile code-review gitlab-ci-mode gitlab-ci-mode-flycheck org-caldav use-package elfeed markdown-mode mocha org-sync csv csv-mode gnuplot-mode gnuplot citeproc citeproc-org org-sidebar latex-extra auctex org-pomodoro pomodoro twig-mode yaml-mode org-journal command-log-mode which-key smart-tabs-mode json-mode web-beautify ac-js2 origami eslint-fix js2-closure xref-js2 js2-refactor npm npm-mode magit git phpstan phpactor php-refactor-mode php-mode evil flycheck projectile code-review gitlab-ci-mode gitlab-ci-mode-flycheck org-caldav use-package elfeed markdown-mode mocha org-sync csv csv-mode gnuplot-mode gnuplot citeproc citeproc-org org-sidebar latex-extra auctex org-pomodoro pomodoro twig-mode yaml-mode org-journal command-log-mode which-key smart-tabs-mode json-mode web-beautify ac-js2 origami eslint-fix js2-closure xref-js2 js2-refactor npm npm-mode magit git phpstan phpactor php-refactor-mode php-mode evil))
 '(pomodoro-work-time 30)
 '(python-indent-def-block-scale 1)
 '(python-indent-guess-indent-offset nil)
 '(python-indent-offset 4)
 '(python-shell-interpreter "python3")
 '(tab-stop-list nil)
 '(tab-width 4)
 '(truncate-lines t)
 '(user-full-name "Clément Cartier")
 '(user-mail-address "contact@catgolin.eu"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(whitespace-tab ((t (:foreground "#636363")))))
