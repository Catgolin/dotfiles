(require 'oc-csl)

;; Keys bindings
(global-set-key (kbd "C-'") 'org-cycle-agenda-files)

;;;;
;; Workflow

;; Todo keys
(setq org-todo-keywords
	  '((sequence "WAITING(w)" "TODO(t)" "NEXT(n)" "APPOINTMENT(a)" "|" "DONE(d)")))

;; Capture distractions
(global-set-key (kbd "C-c c") 'org-capture)

;; Bibliography
;; (setq org-cite-global-bibliography '("~/org/bibliography.bib"))

;; Indentation
(add-hook 'org-mode-hook 'org-indent-mode)

;; Toggle lines
(add-hook 'org-mode-hook 'visual-line-mode)

;; Refiling
;; Source: https://stackoverflow.com/questions/33885244/emacs-org-mode-how-can-i-refile-a-subtree-to-a-new-file
(require 'org-element)
(defun my/org-file-from-tree (&optional name)
  (interactive "P")
  (let ((filename (expand-file-name
				  (read-file-name "New file name: "
								  default-directory))))
	(make-empty-file filename)
	(org-back-to-heading)
	(org-refile nil nil (list nil filename))))
;; Refile while giving a path (source: https://www.reddit.com/r/orgmode/comments/10dvpef/is_there_a_way_to_directly_provide_a_file_path_to/)
(defun my/org-refile-path ()
  (interactive)
  (let ((filepath
		 (read-file-name "Where to refile: ")))
	(org-refile nil nil (list nil filepath))))

;; Koma-Script for letters
(eval-after-load 'ox '(require 'ox-koma-letter))
(eval-after-load 'ox-koma-letter
  '(progn
	 (add-to-list 'org-latex-classes
				  '("lettre"
					"\\documentclass[firstfoot=false, enlargefirstpage]\{scrlttr2\}
\\usepackage[french]\{babel\}
\[DEFAULT-PACKAGES]
\[PACKAGES]
\[EXTRA]"))
	 (setq org-koma-letter-default-class "lettre")))
(eval-after-load 'ox-later
  '(add-to-list 'org-latex-packages-alist '("AUTO" "babel" t) t))

;; LaTeX_CLASS
(unless (boundp 'org-export-latex-classes)
  (setq org-export-latex-classes nil))

;; Insert code (from https://gitlab.inria.fr/learninglab/mooc-rr/mooc-rr-ressources/-/blob/master/module2/ressources/rr_org/init.org)

;; Load python (https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (sqlite . t)))

;; ignore headings (https://emacs.stackexchange.com/a/9494/40532)
(defun org-remove-headlines (backend)
  "Remove headlines with :ignoreheading: tag."
  (org-map-entries (lambda () (delete-region (point-at-bol) (point-at-eol)))
                   "ignoreheading"))

(add-hook 'org-export-before-processing-hook #'org-remove-headlines)
