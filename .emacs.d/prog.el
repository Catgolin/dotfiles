(add-hook 'js2-mode-hook 'ac-js2-setup-auto-complete-mode)

;;; HTML and twig
(add-to-list 'auto-mode-alist '("\\.html$" . twig-mode))
(add-to-list 'auto-mode-alist '("\\.twig$" . twig-mode))

;;;;;
;; 1 PHP
;;
(autoload 'php-mode "php-mode" "Major mode for editing PHP code." t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))

;;;;
;; 2 JavaScript
;;
;; JavaScrip, Json and npm modes
;;
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.mjs$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . json-mode))
(add-hook 'js-mode-hook 'js2-minor-mode)
(add-hook 'js-mode-hook 'ac-js2-mode)
(add-hook 'js-mode-hook 'npm-mode)

;; Syntax checking in JS mode with flycheck minor mode
(require 'flycheck)
(add-hook 'js-mode-hook (lambda() (flycheck-mode t)))
;; Ignore "no side effect" warnings when in test files
(with-eval-after-load "js2-mode"
  (setq-default js2-ignored-warnings '())
  (make-variable-buffer-local 'js2-ignored-warnings)
  (defun my-js2-test-file-setup()
    (when (string-match-p ".test.js$" (buffer-file-name))
      (push "msg.no.side.effects" js2-ignored-warnings)))
  (add-hook 'js2-mode-hook 'my-js2-test-file-setup))

;; Using jslint to prettify source codes, and mapping it to C-c b
(require 'web-beautify)
(eval-after-load 'js2-mode
  '(define-key js2-mode-map (kbd "C-c b") 'web-beautify-js))
(eval-after-load 'json-mode
  '(define-key json-mode-map (kbd "C-c b") 'web-beautify-js))
(eval-after-load 'web-mode
  '(define-key web-mode-map (kbd "C-c b") 'web-beautify-html))
(eval-after-load 'sgml-mode
  '(define-key html-mode-map (kbd "C-c b") 'web-beautify-html))
(eval-after-load 'twig-mode-map
  '(define-key html-mode-map (kbd "C-c b") 'web-beautify-html))
(eval-after-load 'css-mode
  '(define-key css-mode-map (kbd "C-c b") 'web-beautify-css))

;;;;;;
;; 3 Python
;;
;; Configure flymake for Python
(when (load "flymake" t)
  (defun flymake-pylint-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list "epylint" (list local-file))))
  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.py\\'" flymake-pylint-init)))

;; Set as a minor mode for Python
(add-hook 'python-mode-hook '(lambda () (flymake-mode)))

;; Bibtex
(eval-after-load 'bibtex-mode-map
  '(define-key bibtex-mode-map (kbd "C-c b") 'bibtex-reformat))
