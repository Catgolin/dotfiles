;; Indent with tabs in javaScript mode
;; Indent with 4 spaces in php and lisp modes
(smart-tabs-insinuate 'c 'javascript)
(setq custom-tab-width 4)
(defun disable-tabs() (setq indent-tabs-mode nil))
(defun enable-tabs()
  (setq indent-tabs-mode t)
  (setq tab-width custom-tab-width)
  (setq c-basic-offset tab-width)
  )
(add-hook 'prog-mode-hook 'enable-tabs)
(add-hook 'php-mode-hook 'disable-tabs)
(add-hook 'lisp-mode-hook 'disable-tabs)
(add-hook 'twik-mode-hook 'disable-tabs)
(add-hook 'python-mode-hook 'disable-tabs)

;; Show tabs
(global-whitespace-mode)
(setq whitespace-style '(face tabs tab-mark trailing))
(setq whitespace-display-mappings '((tab-mark 9 [124 9])))
