(when window-system
  (menu-bar-mode 0) ;; Removes menu bar on top
  (tool-bar-mode 0) ;; Removes big tool bar on top on the screen
  (scroll-bar-mode 0))

(setq inhibit-splash-screen t) ;; Removes startup screen
(defun startup-agenda ()
  "Opens the agenda in full mode."
  (org-agenda nil "n")
  (delete-other-windows))
(add-hook 'after-init-hook 'startup-agenda) ;; Displays the org agenda
