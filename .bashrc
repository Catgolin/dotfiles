#
# ~/.bashrc
#

source ~/.bash_git

alias dotfiles='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
PS1='\n\t: \[\e[1;31m\]\u@\h\[\e[0;32m\]>\[\e[1;33m\]\W\[\e[1;35m\]$(__git_ps1 " (%s)")\[\e[0;39m\]\$\n'

wiki() {
	search_term="${1}"
	w3m https://wiki.archlinux.org/index.php?search=${search_term}
}

cemacs() {
	emacsclient -c -e "(load (expand-file-name \"startup.el\" user-emacs-directory))"
}

if [ -d "/home/linuxbrew/" ]; then
	eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
else
	eval "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi
