;; Configuration file by Catgolin
;; contact@catgolin.eu
;;

;; Make variable automatically updated by Custom
;; live in the emacs.d/custom.el file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; Loading org-mode
;; See https://orgmode.org/org.html#Installation
;; Downloaded from https://git.savannah.gnu.org/git/emacs/org-mode.git
(add-to-list 'load-path "~/src/org-mode/lisp")
;; Configure org-mode
(when (file-exists-p (expand-file-name "org.el" user-emacs-directory))
  (load (expand-file-name "org.el" user-emacs-directory)))

;; Loading package manager
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; Programming languages environnement configuration
(when (file-exists-p (expand-file-name "prog.el" user-emacs-directory))
  (load (expand-file-name "prog.el" user-emacs-directory)))

;; Indentation
(when (file-exists-p (expand-file-name "indent.el" user-emacs-directory))
  (load (expand-file-name "indent.el" user-emacs-directory)))

;; Startup
(when (file-exists-p (expand-file-name "startup.el" user-emacs-directory))
  (load (expand-file-name "startup.el" user-emacs-directory)))

;; Miscelanious
(when (file-exists-p (expand-file-name "misc.el" user-emacs-directory))
  (load (expand-file-name "misc.el" user-emacs-directory)))
